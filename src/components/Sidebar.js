import React, { useState } from 'react';
import styled from 'styled-components';
import MenuIcon from '@material-ui/icons/Menu';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

import { SidebarData } from '../data/SidebarData'

const Nav = styled.div`
    background-color: white;
    height: 50px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
`
const NavIcon = styled.div`
    margin-left: 1.5rem;
    font-size: 2rem;
    height: 40px;
    display: flex;
    justify-content: flex-start;
    align-items: center;

    :hover {
        cursor: pointer;
        color: red;
    }
`

const Sidenav = styled.nav`
    padding-top: 15px;
    height: 100vh;
    width: 220px;
    background-color: white;
    border-right: 3px solid lightgray;
    display: flex;
    justify-content: center;
    position: fixed;
    top: 0;
    left: ${({ sidebar }) => (sidebar ? '0' : '-100%')};
    transition: 350ms;
    z-index: 10;
`

const SidebarWrap = styled.div`
    width: 100%;
`

const SidebarList = styled.ul`
    height: auto;
    width: 100%;
    padding: 0;

    .row{
        width: 100%;
        height: 60px;
        list-style-type: none;
        margin: 0;
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
        font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Sans', Arial, sans-serif;

        :hover {
            cursor: pointer;
            background-color: lightgray;
            color: red;
            border-left: 5px solid gray;
        }


    }

    #active {
            background-color: gray;
            color: white;
        }

`

const SideIcon = styled.div`
    flex: 30%;
    display: grid;
    place-items: center;
`

const SideTitle = styled.div`
    flex: 70%;
`

function Sidebar() {

    const [ sidebar, setSidebar ] = useState(false);

    const showSidebar = () => setSidebar(!sidebar)

    return (
        <>
            <Nav>
                <NavIcon>
                    <MenuIcon onClick={showSidebar} />
                </NavIcon>
            </Nav>
            <Sidenav sidebar={sidebar}>
                <SidebarWrap>
                    <NavIcon>
                        <HighlightOffIcon onClick={showSidebar} />
                    </NavIcon>
                    <SidebarList>
                        {SidebarData.map((val, key) => {
                            return (
                                <li 
                                    key={key}
                                    className='row'
                                    id={window.location.pathname === val.link
                                        ? "active"
                                        : ""}
                                    onClick={() => {window.location.pathname = val.link;
                                    }}
                                > 
                                    <SideIcon>{val.icon}</SideIcon>
                                    <SideTitle>{val.title}</SideTitle> 
                                </li>
                            )
                        })}
                    </SidebarList>
                </SidebarWrap>
            </Sidenav>
        </>
    )
}

export default Sidebar
