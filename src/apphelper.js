module.exports = {
    getAccessToken: () => localStorage.getItem('token'),
    toJSON: res => res.json()
}