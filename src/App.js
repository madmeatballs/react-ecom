import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
import './App.css';
import Sidebar from './components/Sidebar';
import { Router, Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import orders from './pages/Orders';
import products from './pages/Products';
import addProducts from './pages/AddProducts';
import editProduct from './pages/EditProduct';

import { UserProvider } from './UserContext';
import History from './components/History';


function App() {

	const [user, setUser] = useState({
		isAdmin: null,
		firstName: null,
		lastName: null
	})

	const unSetUser = () => {
		localStorage.clear()
		setUser({
		isAdmin: null,
		firstName: null,
		lastName: null
		})
	}

  return (
    <>
      <UserProvider value={{ user, setUser, unSetUser }}>
        <Router history={History}>
          <Sidebar />
          <Container className='my-3'>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/products' component={products} />
              <Route path='/add-product' component={addProducts} />
              <Route path='/edit-product' component={editProduct} />
              <Route path='/orders' component={orders} />
            </Switch>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
