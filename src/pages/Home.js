import React from 'react'
import styled from 'styled-components';

const Styles = styled.div`

.home {
	background-color: white;
	border-radius: 5px;
	padding: 30px;
	width: 50vw;
	margin: auto;
}

`

const Home = () => {
    return (
        <Styles>
            <div className='home'>
                <h1 className='text-center'>Welcome to Flower Admin</h1>
                <p className='text-center'>This is for demonstration purposes only.</p>
                <p className='text-center'>Please click on the menu button on the upper left corner to access the sidebar nav</p>
            </div>
        </Styles>
    )
}

export default Home
