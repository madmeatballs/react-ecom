import React, {useEffect, useState} from 'react'
import styled from 'styled-components';
import apphelper from '../apphelper';
import { Table } from 'react-bootstrap';

const OrdCont = styled.div`

	background-color: white;
	border-radius: 5px;
	padding: 30px;
	width: 50vw;
	margin: auto;

`

function Orders() {

    const [ orders, setOrders ] = useState([])

    useEffect(() => {
        const payload = {
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
                'Authorization': `Bearer ${apphelper.getAccessToken()}`
            }
        }

        fetch('http://localhost:4000/api/orders/', payload)
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setOrders(data)
            })

    }, []);

    function createTable() {

        return (
            orders.map((item) => {
                return (
                    <tr key={item._id}>
                        <td>{item._id}</td>
                        <td>{item.product_id}</td>
                        <td>{item.price}</td>
                    </tr>
                )})
        )
    }

    return (
        <OrdCont>
        <h3>Orders</h3>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>Product Name</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody> 
                    {createTable()}
                </tbody>
            </Table>
            <p>Total Orders: {orders.length}</p>
        </OrdCont>
    )
}

export default Orders
