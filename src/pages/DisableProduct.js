import React, { useEffect } from 'react';
import Swal from 'sweetalert2';

function DisableProduct() {

        
        useEffect(() => {
            const params = new URLSearchParams(location.search);
            const q = params.get('q');
            console.log(q)
            setPid(q)
    
            //eslint-disable-next-line
    
            const payload = {
                method: 'PATCH',
                headers: {
                    'Content-Type':'application/json',
                    'Authorization': `Bearer ${apphelper.getAccessToken()}`
                }
            }
    
            fetch(`http://localhost:4000/api/products/dis/${pid}`, payload)
                .then(res => {
                    return res.json()
                }).then(data => {
                    if(data === true) {
                        Swal.fire({
                            icon: 'info',
                            title: 'Course Disabled!',
                            text: `Course with id: ${courseId} has been disabled!`
                        })
                        window.location.replace('/products');
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Uh-oh!',
                            text: `Something went wrong!`
                        })
                    }
                })
        }, []);
    
    return (
        <div>
            Disabling product
        </div>
    )
}

export default DisableProduct
