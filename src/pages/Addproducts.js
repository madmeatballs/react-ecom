import React, { useState } from 'react';
import styled from 'styled-components';
import { Form, Button  } from 'react-bootstrap';
import apphelper from '../apphelper';
import Swal from 'sweetalert2'

const FormCont = styled.div`

	background-color: white;
	border-radius: 5px;
	padding: 30px;
	width: 50vw;
	margin: auto;

`

function AddProducts() {
    const [ name, setName ] = useState('');
    const [ desc, setDesc ] = useState('');
    const [ qty, setQty ] = useState(0);
    const [ price, setPrice ] = useState(0);


    const createProduct = (e) => {
        e.preventDefault();

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${apphelper.getAccessToken()}`
            },
            body: JSON.stringify({
                name: name,
                description: desc,
                quantity: qty,
                price: price

            })
        }

        fetch('http://localhost:4000/api/products/add', payload)
            .then(apphelper.toJSON)
            .then(data => {
                if(data === true){
                    Swal.fire({
                        icon: "success",
                        title: "Product added successfully",
                        text: "Thank you!"
                    })
                }else{
                    Swal.fire({
                        icon: "error",
                        title: "Failed to add Product",
                        text: "Something went wrong"
                    })
        
                }
            })
    }

    return (
        <FormCont>
        <h3>Add Product</h3>
            <Form onSubmit={ (e) => createProduct(e) }>
                <Form.Group controlId="productName">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control type="text" value={ name } onChange={ (e)  => setName(e.target.value)} required />
                </Form.Group>
                <Form.Group controlId="productDesc">
                    <Form.Label>Product Description</Form.Label>
                    <Form.Control as="textarea" rows={3} value={ desc } onChange={ (e)  => setDesc(e.target.value)} required />
                </Form.Group>
                <Form.Group controlId="productquantity">
                    <Form.Label>Quantity</Form.Label>
                    <Form.Control type="number" value={ qty } onChange={ (e)  => setQty(e.target.value)} required />
                </Form.Group>
                <Form.Group controlId="productprice">
                    <Form.Label>Price</Form.Label>
                    <Form.Control type="number" value={ price } onChange={ (e)  => setPrice(e.target.value)} required />
                </Form.Group>

                <Button variant="primary" type="submit" className='mt-3'>
                    Submit
                </Button>
            </Form>
        </FormCont>
    )
}

export default AddProducts
