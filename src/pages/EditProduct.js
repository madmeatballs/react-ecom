import React, { useState, useEffect } from 'react'
import styled from 'styled-components';
import { Form, Button, Table  } from 'react-bootstrap';
import apphelper from '../apphelper';
import Swal from 'sweetalert2';

const FormCont = styled.div`

	background-color: white;
	border-radius: 5px;
	padding: 30px;
	width: 50vw;
	margin: auto;

`

const ProdDetails = styled.div`

    background-color: white;
	border-radius: 5px;
	padding: 30px;
	width: 50vw;
    margin: 20px auto;


`

function EditProduct({location}) {

    const [ name, setName ] = useState('');
    const [ desc, setDesc ] = useState('');
    const [ qty, setQty ] = useState(0);
    const [ price, setPrice ] = useState(0);
    const [ status, setStatus ] = useState('Available');

    const [ pid, setPid ] = useState('');

    const [ item, setItem ] = useState('');

    useEffect(() => {
        
        const params = new URLSearchParams(location.search);
        const q = params.get('q');
        console.log(q)
        setPid(q)

        //eslint-disable-next-line
    }, []);

    const getItem = () => {
        const payload = {
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
                'Authorization': `Bearer ${apphelper.getAccessToken()}`
            }
        }

        fetch(`http://localhost:4000/api/products/${pid}`, payload)
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setItem(data)
            })

    }

    const editProduct = (e) => {
        e.preventDefault();

        const payload = {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${apphelper.getAccessToken()}`
            },
            body: JSON.stringify({
                name: name,
                description: desc,
                quantity: qty,
                price: price,
                status: status
            })
        }

        fetch(`http://localhost:4000/api/products/edi/${pid}`, payload)
            .then(apphelper.toJSON)
            .then(data => {
                if(data === true){
                    Swal.fire({
                        icon: "success",
                        title: "Product successfully edited",
                        text: "Thank you!"
                    })
                    setPid('');
                }else{
                    Swal.fire({
                        icon: "error",
                        title: "Failed to edit Product",
                        text: "Something went wrong"
                    })
        
                }
            })
    }

    return (
        <>
            <ProdDetails>
                <h3>Product Details:</h3>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody> 
                        <tr>
                            <td>{item.name}</td>
                            <td>{item.description}</td>
                            <td>{item.quantity}</td>
                            <td>{item.price}</td>
                            <td>{item.status}</td>
                        </tr>
                    </tbody>
                </Table>
            </ProdDetails>

            <FormCont>
                <h3>Edit Product</h3>
                <Form onSubmit={ (e) => editProduct(e) }>
                    <Form.Group controlId="productName">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control type="text" placeholder="Product Name" value={ name } onChange={ (e) => setName(e.target.value) } required />
                    </Form.Group>
                    <Form.Group controlId="productDesc">
                        <Form.Label>Product Description</Form.Label>
                        <Form.Control as="textarea" rows={3} placeholder='Prod Description' value={ desc } onChange={ (e) => setDesc(e.target.value) } required />
                    </Form.Group>
                    <Form.Group controlId="productquantity">
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control type="number" placeholder="0" value={ qty } onChange={ (e) => setQty(e.target.value) } required />
                    </Form.Group>
                    <Form.Group controlId="productprice">
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="number" placeholder="0" value={ price } onChange={ (e) => setPrice(e.target.value) } required />
                    </Form.Group>
                    <Button variant="primary" type="submit" className='mt-3'>
                        Submit
                    </Button>
                </Form>
            </FormCont>
        </>
    )
}

export default EditProduct
