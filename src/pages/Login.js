import React, { useEffect, useState, useContext } from 'react'
import { Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import styled from 'styled-components';

const Styles = styled.div`

.login-form {
	background-color: white;
	border-radius: 5px;
	padding: 30px;
	width: 50vw;
	margin: auto;
}

`

function Login() {

    let history = useHistory();

    const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
    const [ active, setActive ] = useState(false);
	const { setUser } = useContext(UserContext);

    useEffect(() => {
        if(email.length !== 0 || password.length !== 0) {
            setActive(false);
        } else {
            setActive(true);
        }
    },[email,password])
	  
	 const retrieveUserDetails = (accessToken) => {
		const options = {
			headers: { Authorization: `Bearer ${accessToken}`}
		}
		fetch(`http://localhost:4000/api/users/details`, options).then((response) => response.json()).then(data => {
			setUser({ id: data._id });
            history.push('/products');
		})
	 }
   
	 function login(e){
	   e.preventDefault()
	   fetch(`http://localhost:4000/api/users/login`, {
		   method: 'POST',
		   headers: {
			   'Content-Type': 'application/json'
		   },
		   body: JSON.stringify({
			   email: email,
			   password: password
		   })
	   }).then(res => res.json()).then(data => {
		   if(typeof data.accessToken !== "undefined") {
			   localStorage.setItem('token', data.accessToken)
			   retrieveUserDetails(data.accessToken)
			   Swal.fire({
				   icon: 'success',
				   title: 'Successfully Logged In'
			   });
			
		   } else {
			  Swal.fire('Login Error', 'You may have registered using a different login procedure', 'error')
		   }
	   })
	 }
    
    return (
        <>
            <Styles>
            <div className='login-form'>
			<h3>Login</h3>
			<p>Credentials provided in email</p>
                <Form onSubmit={(e) => login(e)}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email"
                        value={email}
                        onChange={(e)=> setEmail(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password"
                        value={password}
                        onChange={(e)=> setPassword(e.target.value)} />
                    </Form.Group>

                    <Button variant="primary" type="submit" disabled={active} className='mt-3'>
                        Login
                    </Button>
                </Form>
            </div>
            </Styles>
        </>
    )
}

export default Login;
