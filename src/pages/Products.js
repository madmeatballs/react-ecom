import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'
import styled from 'styled-components';
import apphelper from '../apphelper';
import { Card } from 'react-bootstrap';

const ProdCont = styled.div`

	background-color: white;
	border-radius: 5px;
	padding: 30px;
	width: 50vw;
	margin: auto;

`

function Products() {

    const [ products, setProducts ] = useState([])

    useEffect(() => {
        const payload = {
            method: 'GET',
            headers: {
                'Content-Type':'application/json',
                'Authorization': `Bearer ${apphelper.getAccessToken()}`
            }
        }

        fetch('http://localhost:4000/api/products/', payload)
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setProducts(data)
            })

    }, []);

    function createCard() {

        return (
            products.map((item) => {
            return (
                <Card className='mt-2' key={item._id}>
                    <Card.Body>
                    <Card.Title>{item.name}</Card.Title>
                    <Card.Subtitle className="mb-2">Quantity: {item.quantity}</Card.Subtitle>
                    <Card.Subtitle className="mb-2">Price: {item.price}</Card.Subtitle>
                    <Card.Subtitle className="mb-2">Status: {item.status}</Card.Subtitle>
                        <Card.Text>{item.description}</Card.Text>
                    </Card.Body>
                    <Card.Footer>
                        <Link 
                            className='btn btn-secondary mx-2' 
                            data-pid={item._id}
                            to={`/edit-product?q=${item._id}`}
                        >
                            Edit
                        </Link>
                        <Link 
                            className='btn btn-secondary mx-2' 
                            data-pid={item._id}
                            to={`/disable-product?q=${item._id}`}
                        >
                            Disable
                        </Link>
                        <Link 
                            className='btn btn-secondary mx-2' 
                            data-pid={item._id}
                            to={`/delete-product?q=${item._id}`}
                        >
                            Delete
                        </Link>
                    </Card.Footer>
                </Card>
            )})
        )
    }

    return (

            <>
                <ProdCont>
                <h3>Products</h3>
                    {createCard()}
                </ProdCont>
            </>

    )
}

export default Products
