import React from 'react';
import HomeIcon from '@material-ui/icons/Home';
import LocalFloristIcon from '@material-ui/icons/LocalFlorist'
// import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import EcoIcon from '@material-ui/icons/Eco';
// import VpnKeyIcon from '@material-ui/icons/VpnKey';

export const SidebarData = [
    {
        title: 'Home',
        icon: <HomeIcon />,
        link: '/',
        status: 'logged-in'
    },
    {
        title: 'Products',
        icon: <LocalFloristIcon />,
        link: '/products',
        status: 'logged-in'
    },
    {
        title: 'Add Product',
        icon: <EcoIcon />,
        link: '/add-product',
        status: 'logged-in'
    },
    {
        title: 'Orders',
        icon: <LocalShippingIcon />,
        link: '/orders',
        status: 'logged-in'
    }
    // {
    //     title: 'Logout',
    //     icon: <MeetingRoomIcon />,
    //     link: '/logout',
    //     status: 'logged-in'
    // },
    // {
    //     title: 'Login',
    //     icon: <VpnKeyIcon />,
    //     link: '/login',
    //     status: 'logged-out'
    // }
]

